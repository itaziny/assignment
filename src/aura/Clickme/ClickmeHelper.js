({
	handleOpenModal : function(component, helper, openModal) {
		component.set("v.openModal", openModal);
	},
    
    handleShowText : function(component, helper, displayText) {
		component.set("v.showText", displayText);
	}
})