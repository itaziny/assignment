({
	handleClickme : function(component, event, helper) {
		helper.handleOpenModal(component, helper, true);
	},
    
    handleConfirm : function(component, event, helper) {
        var textConfirm = component.get("v.confirmText");
		component.set("v.textResult", textConfirm);
        helper.handleShowText(component, helper, true);
        helper.handleOpenModal(component, helper, false);
	},
    
    handleCancel : function(component, event, helper) {        
		var textCancel = component.get("v.cancelText");
		component.set("v.textResult", textCancel);
        helper.handleShowText(component, helper, true);
        helper.handleOpenModal(component, helper, false);
        console.log('handleCancel function: '+textCancel);
	},
    
    closeModal: function(component, event, helper) {
        component.set("v.textResult", "");
        helper.handleShowText(component, helper, false);
		helper.handleOpenModal(component, helper, false);
	}
})