/**
* @File Name:	NF_AccountTrigger_Example.trigger
* @Description: Trigger for Account Object  
* @Author:   	Ihssan Taziny
* @Group:   	Trigger
* @Modification Log	:
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2020-02-04  Ihssan    Created the file/class
*/
trigger NF_AccountTrigger_Example on Account (
	before insert, 
	before update, 
	before delete, 
	after insert, 
	after update, 
	after delete, 
	after undelete) {
	NF_TriggerFactory.CreateHandlerAndExecute(Account.sObjectType);
}