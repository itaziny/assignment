/**
* @File Name:   NF_AccountTriggerHandler_Test.cls
* @Description: Test class for Account handler
* @Author:      Ihssan Taziny
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2020-02-05  Ihssan    Created the file/class
*/
@isTest
private  class NF_AccountTriggerHandler_Test {
    
    @IsTest
    static void testCreateAccount(){
        
        Test.startTest();

            Account acc1 = new Account(Name = 'test account 1');
            Account acc2 = new Account(Name = 'test account 2');
            Account acc3 = new Account(Name = 'test account 3');
            insert new List<Account>{acc1, acc2, acc3};

        Test.stopTest();
        
        // Verify Creation of Contact with the same name as related accounts
        List<Contact> c1 = [select Id, Lastname from Contact where AccountId = :acc1.Id];
        List<Contact> c2 = [select Id, Lastname from Contact where AccountId = :acc2.Id];
        List<Contact> c3 = [select Id, Lastname from Contact where AccountId = :acc3.Id];

        System.assertEquals(1, c1.size());
        System.assertEquals(acc1.Name, c1[0].Lastname);
        System.assertEquals(1, c2.size());
        System.assertEquals(acc2.Name, c2[0].Lastname);
        System.assertEquals(1, c3.size());
        System.assertEquals(acc3.Name, c3[0].Lastname);
    }

	@IsTest
	static void testBulkAccounts(){
		List<Account> accs = new List<Account>();

		Test.startTest();

			for(Integer i = 1; i <= 400; i++){
				accs.add(new Account(Name = 'test Account '+i));
			}
			insert accs;
		
		Test.stopTest();
		
		// Verify created contacts
        List<Contact> contacts = [select Id, Lastname from Contact];
		System.assertEquals(accs.size(), contacts.size());

	}
}