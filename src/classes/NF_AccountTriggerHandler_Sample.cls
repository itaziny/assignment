/**
* @File Name:   NF_AccountTriggerHandler_Sample.cls
* @Description:   
* @Author:      Ihssan Taziny
* @Group:       Apex
* @Modification Log :
*-------------------------------------------------------------------------------------
* Ver       Date        Author      Modification
* 1.0       2020-02-04  Ihssan    Created the file/class
*/
public with sharing class NF_AccountTriggerHandler_Sample extends NF_AbstractTriggerHandler {
	
	List<Contact> contactsToCreate = new List<Contact>();	// List of contacts to create from accounts

	public override void beforeUpdate(){

	}

	public override void afterUpdate(){

	}

	public override void beforeInsert(){

	}

	public override void afterInsert(){
		// for each new Account create a contact with the same name
		for(Account acc : (List<Account>)Trigger.new){
			contactsToCreate.add(new Contact(Lastname = acc.Name, AccountId = acc.Id));
		}
	}

	public override void afterDelete(){

	}

	public override void andFinally(){
		if(Trigger.isInsert && Trigger.isAfter){
			if(!contactsToCreate.isEmpty()){
				try {
					insert contactsToCreate;

				} catch (Exception ex) {
					System.debug('NF_AccountTriggerHandler_Sample - andFinally - Exception on contacts creation: '+ex.getMessage());
				}
			}
		}
	}
}